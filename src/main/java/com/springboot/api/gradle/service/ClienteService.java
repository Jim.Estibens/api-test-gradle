package com.springboot.api.gradle.service;

import java.util.List;

import com.springboot.api.gradle.model.Cliente;

public interface ClienteService {
	
	List<Cliente> getAllClientes();
	Cliente getCliente(Integer id);
	void saveCliente(Cliente persona);
	void deleteCliente(Integer id);
	
}
