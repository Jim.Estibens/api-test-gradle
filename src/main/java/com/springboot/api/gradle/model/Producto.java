package com.springboot.api.gradle.model;

public class Producto {
	
	private Integer id;
	private String descripcion;
	private String categoria;
	private Double precioUnitario;
	private Integer stockActual;
	private Integer stockMinimo;
	private String estado;
	
	public Producto() {
	
	}
	
	public Producto(Integer id, String descripcion, String categoria, Double precioUnitario, Integer stockActual,
			Integer stockMinimo, String estado) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.categoria = categoria;
		this.precioUnitario = precioUnitario;
		this.stockActual = stockActual;
		this.stockMinimo = stockMinimo;
		this.estado = estado;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public Double getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public Integer getStockActual() {
		return stockActual;
	}
	public void setStockActual(Integer stockActual) {
		this.stockActual = stockActual;
	}
	public Integer getStockMinimo() {
		return stockMinimo;
	}
	public void setStockMinimo(Integer stockMinimo) {
		this.stockMinimo = stockMinimo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Producto [id=" + id + ", descripcion=" + descripcion + ", categoria=" + categoria + ", precioUnitario="
				+ precioUnitario + ", stockActual=" + stockActual + ", stockMinimo=" + stockMinimo + ", estado="
				+ estado + "]";
	}
		
	
		
	
}
